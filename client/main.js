import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

import ImageList from './components/ImageList';

class App extends Component{
    constructor( props ){
        super(props);

        this.state = {
            images: [],
            error: ''
        };
    }

    componentWillMount(){
        axios.get('https://api.imgur.com/3/gallery/hot/viral/0')
            .then(response => {
                this.setState({...this.state, images: response.data.data, error: '' });
                console.log(response);
            })
            .catch(error => {
                this.setState({...this.state, error: error.message });
                console.log(error.message);
            });
    }

    render() {
        return (
            <div className="container-fluid">
                <ImageList images={ this.state.images } />
            </div>
        )
    }
}

Meteor.startup(() => {
    ReactDOM.render( <App />, document.querySelector('.container'));
});
