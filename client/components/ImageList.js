import React, { Component } from 'react';
import ImageDetail from './ImageDetail';

const ImageList = ( props ) => {
    const validImages = props.images.filter( image => { return !image.is_album });

    const RenderImages = validImages.map( (image, idx) => {
        return <ImageDetail key={'img'+idx} image={ image } />
    });

    return (
        <ul className="media-list list-group">
            { RenderImages }
        </ul>
    )
}

export default ImageList;
